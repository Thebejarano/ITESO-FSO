* <2023-08-14 lun>

semana 1 / Inicio de Curso / Presentacion

Presentacion del profesor
  - credenciales
  - a pesar de que soy el profesor, tambien aprendo de ustedes
  - ustedes quiza saben mas que yo en algunas areas, la idea es que todos aprendamos de todos
  - ademas de los objetivos del curso, como objetivo ideal seria que todos pudieramos contribuir de alguna forma a un projecto del ecosistema opensource
  - idioma? espan~ol o ingles? en la 'vida real' se usa el ingles, yo quisiera que fuera en ingles lo mas posible

Llenado de Cuestionario
  - revision del mismo entre todos

Notas generales del curso

  - la pagina de canvas NO esta actualizada, importe el curso del maestro Jose Luis Elvira
    pero sobre este estoy an~adiendo nuevo material y ajustando fechas. No sera el mismo curso que imparte
    Jose Luis, lo extiendo en algunas areas y son diferentes actividades/tareas.
  - Los videos del maestro Elvira son altamente recomendados sobre todo para aquellos que les guste mas
    aprender con multimedia que con slides.
  - Dos sets de slides, unos en espan~ol (cortos) y otros en ingles (largos). El primero es subconjunto del segundo.
  - OJO soy muchos slides! es mucho material y hay material nada facil!
  - De ser posible, estudien los dos sets!
  - Aula invertida, ustedes estudian los slides y en la clase realizamos actividades
  - vamos a codificar bastante, usaremos en C, Bash y Python
  - vamos a usar mucho herramientas desde la linea de codigo
  - usaremos git para llevar bien cambios y avances


Guia de Aprendizaje

  - lectura de la misma

Configuración, descarga e instalación de herramientas para el curso


* <2023-08-16 mié>

semana 1 / Estructuras de los sistemas de computo y del sistema operativo

primera parte
- repaso breve de los slides
- dudas sobre los slides
- creacion de equipos
- lectura de la actividad y comienzo de ella

segunda parte
- https://www.virtualbox.org/manual/UserManual.html#virt-why-useful
- slide 48 de OSC - Introduction
- instalacion de la maquina virtual


* <2023-08-21 lun>

semana 2 / Procesos Sesion 1

Primera Hora
1. Respuestas de Actividad 1
   grupo 1 responde la pregunta 1, grupo 2 responde la pregunta 2, etc.

2. uso de pandoc con markdown
   https://www.markdownguide.org/cheat-sheet/
   compartir pantalla y ver actividad1.md, usar pandoc para generar pdf

3. Revisar el tema de linkers & loaders

   Linkers and Loaders
   - Slide 32 de chap2.pptx
   - elaborar un 'hello world' en C
   - compilarlo con las instrucciones del slide 2.33 de chap2.pptx

4. revisar el tema de system calls

   Slide 7 y 17 del chap2.pptx
   Slide 23 del 01 Resumen capitulos 1 y 2
   ejecutarlo 
   ejecutarlo bajo strace

Segunda Hora

5. Revision de Slides 02 Procesos
   - detenerse en Concepto de Proceso
   - ver procesos que estan corriendo en maquinas huesped (Resource Monitor en windows)
   - ver los procesos que estan corriendo en la maquina virtual
   - terminar planificadores con 1 y 2 cpus
   - en la maquina virtual, monitorial los procesos con el comando perf
   - instalar el siguiente paquete
     sudo apt install linux-tools-common
     perf timechart record




* <2023-08-23 mié>

semana 2 / Procesos Sesion 2

1. Revision actividad de sesion previa (tarea)

2. Revision de los slides de la sesion del dia

3. Revision de examen previo a sesion del dia

4. revision de repositorio en gitlab

5. pedirles que clonen el repo dentro de sus maquinas virtuales

6. pedirles que compilen el ejemplo 1.-Procesos/ejemplo1.c


* <2023-08-28 lun>


semana 3 / Procesos - Cooperation & Communication

1. Revision de Actividad 1.2 - Intro & Services
   - todos aquellos que hayan recibido comentarios, se discutiran en clase
   - Mauricio Martin del Campo, agregarlo a un equipo
   - Explain how the Linux kernel variables HZ and jiffies can be used to
     determine the number of seconds the system has been running since it
     was booted.
     - man 7 time - time and timers
       - HZ = 60 /* 100 cycles / second */
       - jiffy_duration = 1 / HZ /* duration of each cycle, in seconds)
       - booted elapsed time = current_number_of_jiffies * jiffy_duration

2. Revision de actividad 2.2
   - hacer un fork del repositorio del curso
   - crear un branch con el nombre actividad-2.2
   - colocar el codigo fuente en el folder respectivo ITESO-FSO/activities/2.2-Process-Creation/
   - indicar a git llevar el control de el codigo fuente/archivos con git-add
   - generar un commit
   - generar un Makefile, como ejemplo ver https://www.gnu.org/software/make/manual/html_node/Simple-Makefile.html
   - revisar el comando make

3. Revision los slides cortos de la actividad 3.1 - Process - Cooperation & Communication
   Revision los slides extendidos de la actividad 3.1 - Process - Cooperation & Communication
   

